// Copyright 2019 Levi Durfee

// Package ezrsa _
// * Convert public/private keys to strings
// * Convert keys from strings to rsa pub/priv keys
// * Encrypt/Decrypt using ezrsa structs
package ezrsa

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"math/big"
	"os"
	"strconv"
	"strings"
)

// PubKey represents a RSA Public Key
type PubKey rsa.PublicKey

// Encrypt will encrypt a string using the PubKey.
func (p PubKey) Encrypt(input string) (string, error) {
	rng := rand.Reader
	pk := rsa.PublicKey(p)
	ciphertext, err := rsa.EncryptOAEP(sha256.New(), rng, &pk, []byte(input), nil)
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(ciphertext), nil
}

// PrivKey represents a RSA Private Key
type PrivKey rsa.PrivateKey

// Decrypt will decrypt a string using the PrivKey.
func (p PrivKey) Decrypt(input string) (string, error) {

	cipherText, err := base64.StdEncoding.DecodeString(input)
	if err != nil {
		return "", err
	}

	rng := rand.Reader

	pk := rsa.PrivateKey(p)
	plaintext, err := rsa.DecryptOAEP(sha256.New(), rng, &pk, cipherText, nil)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error from decryption: %s\n", err)
		return "", err
	}

	return string(plaintext), nil
}

// convertPublicKeyToString converts an RSA Public Key to a string.
func convertPublicKeyToString(k PubKey) string {
	return k.N.String() + "," + strconv.Itoa(k.E)
}

// CreatePubKey will convert a string to a PubKey.
func CreatePubKey(input string) (PubKey, error) {
	i := strings.Split(input, ",")

	bigInt := new(big.Int)
	bigInt.SetString(i[0], 10)

	e, err := strconv.Atoi(i[1])
	if err != nil {
		return PubKey{}, err
	}

	return PubKey{
		N: bigInt,
		E: e,
	}, nil
}

// convertPrivateKeyToString converts an RSA Private Key to a string.
func convertPrivateKeyToString(k PrivKey) string {
	pub := convertPublicKeyToString(PubKey(k.PublicKey))
	var primes string
	for _, p := range k.Primes {
		primes += p.String() + "^"
	}
	primes = primes[:len(primes)-1]

	var crt string

	for _, c := range k.Precomputed.CRTValues {
		crt += c.Exp.String() + "^"
		crt += c.Coeff.String() + "^"
		crt += c.R.String() + "^"
	}

	if len(crt) > 0 {
		crt = crt[:len(crt)-1]
	}

	s := pub + "|" + k.D.String() + "|" + primes + "|" + k.Precomputed.Dp.String() + "|" + k.Precomputed.Dq.String() + "|" + k.Precomputed.Qinv.String() + "|" + crt

	if s[len(s)-1:] == "|" {
		s = s[:len(s)-1]
	}

	return s
}

func (p PrivKey) String() string {
	return convertPrivateKeyToString(p)
}

func (p PubKey) String() string {
	return convertPublicKeyToString(p)
}

// CreatePrivKey will convert a string to PrivKey
func CreatePrivKey(input string) (*PrivKey, error) {
	privKey := new(PrivKey)

	// split the string
	kp := strings.Split(input, "|")

	// Get the public key
	pubKey, err := CreatePubKey(kp[0])
	if err != nil {
		return privKey, err
	}

	// Set the public key
	privKey.PublicKey = rsa.PublicKey(pubKey)

	// Set the N field
	biN := new(big.Int)
	biN.SetString(kp[1], 10)
	privKey.D = biN

	// Set the Primes field
	var biPrimes []*big.Int
	for _, p := range strings.Split(kp[2], "^") {
		biP := new(big.Int)
		biP.SetString(p, 10)
		biPrimes = append(biPrimes, biP)
	}
	privKey.Primes = biPrimes

	// Get Precomputed: Dp, Dq, Qinv
	dp := new(big.Int)
	dp.SetString(kp[3], 10)
	privKey.Precomputed.Dp = dp

	dq := new(big.Int)
	dq.SetString(kp[4], 10)
	privKey.Precomputed.Dq = dq

	qinv := new(big.Int)
	qinv.SetString(kp[5], 10)
	privKey.Precomputed.Qinv = qinv

	return privKey, nil
}

// GenerateKey will generate a RSA key of the size param.
func GenerateKey(size int) (PrivKey, PubKey, error) {
	rng := rand.Reader
	k, err := rsa.GenerateKey(rng, size)
	if err != nil {
		return PrivKey{}, PubKey{}, err
	}

	return PrivKey(*k), PubKey(k.PublicKey), nil
}

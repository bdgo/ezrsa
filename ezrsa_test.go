package ezrsa

import (
	"testing"
)

func TestEncryptDecrypt(t *testing.T) {
	pt := "Hello world!"
	pr, pu, _ := GenerateKey(1024)
	encryptedText, _ := pu.Encrypt(pt)

	plainText, _ := pr.Decrypt(encryptedText)

	if pt != plainText {
		t.Error("error")
	}
}

func TestConvertPublicKeyToString(t *testing.T) {
	privKey, pubKey, _ := GenerateKey(1024)

	// Encrypt a string with pubKey first
	const input = "Hiiiii!!!!"

	encryptedInput1, _ := pubKey.Encrypt(input)

	// Convert the PubKey to a string
	pks := convertPublicKeyToString(pubKey)

	// Convert the string back to a PubKey
	pubKey2, _ := CreatePubKey(pks)

	// Encrypt the same input with the PubKey
	encryptedInput2, _ := pubKey2.Encrypt(input)

	// Decrypt both
	dInput1, _ := privKey.Decrypt(encryptedInput1)
	dInput2, _ := privKey.Decrypt(encryptedInput2)

	if dInput1 != dInput2 {
		t.Error("Mismatch")
	}
}

func TestConvertPrivateKeyToString(t *testing.T) {
	privKey, pubKey, _ := GenerateKey(1024)

	// Encrypt a string with pubKey first
	const input = "Hiiiii!!!!"

	encryptedInput1, _ := pubKey.Encrypt(input)

	pks := convertPrivateKeyToString(privKey)
	pk, _ := CreatePrivKey(pks)

	output, err := pk.Decrypt(encryptedInput1)
	if err != nil {
		t.Error(err)
	}

	if output != input {
		t.Error("No match")
	}
}

func TestEverything(t *testing.T) {
	privateKey, publicKey, err := GenerateKey(1024)
	if err != nil {
		t.Error(err)
	}

	secretMessage := "Hello world, let's do dis"

	// Encrypt something
	encryptedMessage, err := publicKey.Encrypt(secretMessage)

	privateKeyString := privateKey.String()
	//publicKeyString := publicKey.String()

	// Reconstruct Private key
	privKey, err := CreatePrivKey(privateKeyString)
	if err != nil {
		t.Error(err)
	}

	decryptedMessage, err := privKey.Decrypt(encryptedMessage)
	if err != nil {
		t.Error(err)
	}

	if decryptedMessage != secretMessage {
		t.Error("decrypted message is wrong")
	}
}

func TestLongString(t *testing.T) {
	privateKey, publicKey, err := GenerateKey(1024)
	if err != nil {
		t.Error(err)
	}

	secretMessage := "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam malesuada porttitor est, non sollicitudin ligula ornare ut. Aenean pellentesque lectus vitae justo luctus consectetur. Pellentesque sed sapien sodales, pharetra quam ac, consequat velit. Nunc quis dui sit amet diam posuere rutrum ac sodales nibh. Sed auctor nisl eleifend, tempor nibh quis, suscipit tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin eget ipsum lorem."

	// Encrypt something
	encryptedMessage, err := publicKey.Encrypt(secretMessage)
	if err != nil {
		t.Error(err)
	}

	privateKeyString := privateKey.String()
	//publicKeyString := publicKey.String()

	// Reconstruct Private key
	privKey, err := CreatePrivKey(privateKeyString)
	if err != nil {
		t.Error(err)
	}

	decryptedMessage, err := privKey.Decrypt(encryptedMessage)
	if err != nil {
		t.Error(err)
	}

	if decryptedMessage != secretMessage {
		t.Error("decrypted message is wrong")
	}
}
